<?php

use app\Lib\Response;
use app\Lib\Route;

#region Root route
Route::get('/', function () {
    (new Response())
        ->redirect(
            route(
                isAuth()
                    ? 'admin'
                    : 'login'
            )
        )
        ->execute();
})->name('root');
#endregion Root route

#region Login routes
Route::get('/login', function () {
    (new Response())
        ->view('login/login.php')
        ->execute();
})->name('login');

Route::get('/changePassword', function () {
    (new Response())
        ->view('login/changePassword.php')
        ->execute();
})->name('changePassword');

Route::get('/resetPassword', function () {
    (new Response())
        ->view('login/resetPassword.php')
        ->execute();
})->name('resetPassword');

Route::post('/login', 'AuthContactsController@login')
    ->name('submitLogin');

Route::post('/changePassword', 'AuthContactsController@changePassword')
    ->name('submitChangePassword');

Route::post('/resetPassword', 'AuthContactsController@resetPassword')
    ->name('submitResetPassword');
#endregion

#region Admin routes
Route::get('/admin', 'AdminController@admin')
    ->name('admin')
    ->protected();

Route::get('/admin/mis', 'AdminController@mis')
    ->name('mis')
    ->protected();

Route::get('/admin/users', function () {
    (new Response())
        ->view('admin/users.php')
        ->execute();
})->name('viewUsers')->protected();

Route::get('/admin/user', 'AdminController@addEditUser')
    ->name('user')
    ->protected();

Route::get('/admin/profile', 'AdminController@editProfile')
    ->name('profile')
    ->protected();

Route::post('/admin/user', 'AdminController@storeUpdateUser')
    ->name('saveUser')
    ->protected();

Route::post('/admin/deleteUser', 'AdminController@deleteUser')
    ->name('deleteUser')
    ->protected();

Route::get('/admin/profile', 'AdminController@updateProfile')
    ->name('saveProfile')
    ->protected();
#endregion