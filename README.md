MIS
---
Last updated: 11 November 2021

Author: [Jakub Kozlowski](kozlows4@cucollege.coventry.ac.uk)

Module: 607IT - Service Management and Support Project


## Description

Landing page for the MIS with user management. 

### Development Environment
#### Required software 
1. Composer
2. NPM 
3. XAMPP (or equivalent, alternatively LAMP setup)
4. IDE of choice
#### Setup
1. Import or create a database
2. Copy "/config/config.php.dir" file to "/config/config.php" and enter local configuration for database access
3. Run "composer install" and "npm install"
4. Run "npm run prod"
5. In web server configuration (assuming Apache) modify or add a virtual host with "DocumentRoot" pointing to the "public" directory of the project (in XAMPP virtual host can be configured in "xampp/apache/conf/extra/httpd-vhosts.conf" file)
6. Add an entry to the "hosts" file to map "ServerName" to the localhost IP address (127.0.0.1)
7. Restart web server
8. Website should be available under "http://set_server_name"