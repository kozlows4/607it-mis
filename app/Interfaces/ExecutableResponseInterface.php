<?php


namespace app\Interfaces;

/**
 * Interface ExecutableResponseInterface
 *
 * @package app\Interfaces
 */
interface ExecutableResponseInterface
{
    /**
     * Send response
     */
    public function execute(): void;
}