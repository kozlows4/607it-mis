<?php


namespace app\Enums;

/**
 * Class MainMenuItems
 *
 * @package app\Enums
 */
class MainMenuItems extends Enumerable
{
    public const MAIN_MENU = 1;
    public const BUSINESS_CONTACTS = 2;
    public const PERSONAL_CONTACTS = 3;
    public const ADD_BUSINESS_CONTACT = 4;
    public const ADD_PERSONAL_CONTACT = 5;
}