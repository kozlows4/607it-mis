<?php


namespace app\Lib;

use app\Lib\Responses\JsonResponse;
use app\Lib\Responses\RedirectResponse;
use app\Lib\Responses\ViewResponse;

/**
 * Class Response
 *
 * @package app\Lib
 */
class Response
{
    public const ROUTING_SUCCESS = 200;
    public const ROUTING_REDIRECT = 302;
    public const ROUTING_ERROR_FORBIDDEN = 403;
    public const ROUTING_ERROR_NOT_FOUND = 404;
    public const ROUTING_ERROR_METHOD_NOT_ALLOWED = 405;

    /**
     * Response status
     *
     * @var int $status
     */
    public int $status;

    /**
     * Response constructor.
     *
     * @param int $status
     */
    public function __construct(int $status = self::ROUTING_SUCCESS)
    {
        $this->status = $status;
    }

    /**
     * Get ViewResponse
     *
     * @param string $view
     * @param array $data
     * @param int $status
     * @param string $header
     * @return ViewResponse|null
     */
    public function view(
        string $view,
        array $data = [],
        int $status = self::ROUTING_SUCCESS,
        string $header = 'Content-Type: text/html'
    ): ViewResponse
    {
        return new ViewResponse($view, $data, $status, $header);
    }

    /**
     * Get ViewResponse short syntax
     *
     * @param string $view
     * @param array $data
     * @param int $status
     * @param string $header
     * @return ViewResponse|null
     */
    public static function viewResponse(
        string $view,
        array $data = [],
        int $status = self::ROUTING_SUCCESS,
        string $header = 'Content-Type: text/html'
    ): ViewResponse
    {
        return (new self())->view($view, $data, $status, $header);
    }

    /**
     * Get JsonResponse
     *
     * @param array $data
     * @param int $status
     * @return JsonResponse
     */
    public function json(array $data = [], int $status = self::ROUTING_SUCCESS): JsonResponse
    {
        return new JsonResponse($data, $status);
    }

    /**
     * Get JsonResponse short syntax
     *
     * @param array $data
     * @param int $status
     * @return JsonResponse
     */
    public function jsonResponse(array $data = [], int $status = self::ROUTING_SUCCESS): JsonResponse
    {
        return (new self())->json($data, $status);
    }

    /**
     * Get RedirectResponse
     *
     * @param string|null $path
     * @return RedirectResponse
     */
    public function redirect(?string $path = null): RedirectResponse
    {
        return new RedirectResponse($path);
    }

    /**
     * Get RedirectResponse short syntax
     *
     * @param string|null $path
     * @return RedirectResponse
     */
    public static function redirectResponse(?string $path = null): RedirectResponse
    {
        return (new self())->redirect($path);
    }

    /**
     * Abort request
     *
     * @param int $errorCode
     * @return ViewResponse|null
     */
    public function abort(int $errorCode): ?ViewResponse
    {
        $view = null;

        switch ($errorCode) {
            case self::ROUTING_ERROR_FORBIDDEN:
                $view = $this->view(
                    'errors/403.php',
                    [],
                    self::ROUTING_ERROR_FORBIDDEN,
                    'HTTP/1.0 403 Forbidden'
                );
                break;
            case self::ROUTING_ERROR_NOT_FOUND:
                $view = $this->view(
                    'errors/404.php',
                    [],
                    self::ROUTING_ERROR_NOT_FOUND,
                    'HTTP/1.0 404 Not Found'
                );
                break;
            case self::ROUTING_ERROR_METHOD_NOT_ALLOWED:
                $view = $this->view(
                    'errors/405.php',
                    [],
                    self::ROUTING_ERROR_METHOD_NOT_ALLOWED,
                    'HTTP/1.0 405 Method Not Allowed'
                );
                break;
            default:
                break;
        }

        return $view;
    }

    /**
     * Abort request short syntax
     *
     * @param int $errorCode
     * @return ViewResponse|null
     */
    public static function abortResponse(int $errorCode): ?ViewResponse
    {
        return (new self())->abort($errorCode);
    }
}