<?php


namespace app\Lib\Responses;


use app\Interfaces\ExecutableResponseInterface;
use app\Lib\Response;

/**
 * Class RedirectResponse
 *
 * @package app\Lib\Responses
 */
class RedirectResponse implements ExecutableResponseInterface
{
    /**
     * @var string $path
     */
    private string $path;

    /**
     * RedirectResponse constructor.
     *
     * @param string|null $path
     */
    public function __construct(?string $path = null)
    {
        $this->path = $path ?? route('root');
    }

    /**
     * @see ExecutableResponseInterface::execute()
     */
    public function execute(): void
    {
        http_response_code(Response::ROUTING_REDIRECT);
        header('Location: ' . $this->path, true, Response::ROUTING_REDIRECT);
    }
}