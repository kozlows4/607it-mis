<?php


namespace app\Lib;


use PDO;
use PDOException;
use PDOStatement;

/**
 * Class DB
 * @package app\Lib
 */
class DB
{
    /**
     * Instance of the class
     *
     * @var DB|null $instance
     */
    private static ?self $instance = null;

    /**
     * PDO connection
     *
     * @var PDO|null $connection
     */
    private ?PDO $connection = null;

    /**
     * Connection established flag
     *
     * @var bool $connected
     */
    private bool $connected = false;

    /**
     * Use instance of the class to execute the database query
     *
     * @param string $query
     * @param array $params
     * @return bool|PDOStatement
     */
    public static function query(string $query, array $params = [])
    {
        return self::getInstance()->queryDb($query, $params);
    }

    /**
     * Get instance of the class
     *
     * @return static
     */
    private static function getInstance(): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * DB constructor.
     */
    private function __construct()
    {
        try {
            $this->connection = new PDO(
                'mysql:host=' . Config::get('DB_HOST')
                    . ';dbname=' . Config::get('DB_NAME')
                    . ';charset=' . Config::get('DB_CHARSET'),
                Config::get('DB_USERNAME'),
                Config::get('DB_PASSWORD'),
                [
                    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES   => false,
                ]
            );

            $this->connected = true;
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), (int) $e->getCode());
        }
    }

    /**
     * Execute databse query
     *
     * @param string $query
     * @param array $params
     * @return bool|PDOStatement
     */
    private function queryDb(string $query, array $params)
    {
        if ($this->connected) {
            try {
                $statement = $this->connection->prepare($query);
                $statement->execute($params);

                return $statement;
            } catch (PDOException $exception) {
                Log::errorLog($exception->getMessage());

                return false;
            }
        } else {
            return false;
        }
    }
}