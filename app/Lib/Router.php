<?php

namespace app\Lib;

use app\Interfaces\ExecutableResponseInterface;

/**
 * Class Route
 *
 * @package app\Lib
 */
class Router
{
    /**
     * List of available routes
     *
     * @var Route[] $routes
     */
    private static array $routes = [];

    /**
     * Add route
     *
     * @param Route $route
     * @return bool
     */
    public static function addRoute(Route $route): bool
    {
        $added = false;

        if ($routeName = $route->getName()) {
            if (self::validateRouteName($routeName)) {
                self::$routes[] = $route;

                $added = true;
            }
        } else {
            if (self::validateRoutePathAndMethod($route->getPath(), $route->getMethod())) {
                self::$routes[] = $route;

                $added = true;
            }
        }

        return $added;
    }

    /**
     * Find route
     *
     * @param string $name
     * @return string|null
     */
    public static function findRoute(string $name): ?string
    {
        $matchingRoute = null;

        foreach (self::$routes as $route) {
            if ($route->getName() === $name) {
                $matchingRoute = $route;

                break;
            }
        }

        return $matchingRoute->getPath();
    }

    /**
     * Validate route's name
     *
     * @param string $name
     * @return bool
     */
    private static function validateRouteName(string $name): bool
    {
        $nameExists = false;

        foreach (self::$routes as $route) {
            if (($routeName = $route->getName()) && $routeName === $name) {
                $nameExists = true;

                break;
            }
        }

        return !$nameExists;
    }

    /**
     * Validate route's path and method
     *
     * @param string $path
     * @param string $method
     * @return bool
     */
    private static function validateRoutePathAndMethod(string $path, string $method): bool
    {
        $routeExists = false;

        foreach (self::$routes as $route) {
            $routePath = $route->getPath();
            $routeMethod = $route->getMethod();

            if ($routePath === $path && $routeMethod === $method) {
                $routeExists = true;
            }
        }

        return !$routeExists;
    }

    /**
     * Handle routing
     *
     * @param Request $request
     */
    public static function handleRouting(Request $request): void
    {
        $response = null;
        $route_match_found = $method_match_found = false;
        $matchingRoute = null;
        $method = strtolower($_SERVER['REQUEST_METHOD']);

        $path = explode('?', $_SERVER['REQUEST_URI'])[0] ?? '/';

        if ($path !== '/') {
            if (substr($path, -1) === '/') {
                $path = substr($path, 0, -1);
            }
        }

        foreach (self::$routes as $route) {
            if ($route->getPath() === $path) {
                $route_match_found = true;

                if ($route->getMethod() === $method) {
                    $matchingRoute = $route;
                    break;
                }
            }
        }

        if (isset($matchingRoute)) {
            if ($matchingRoute->getPath() === '/' && isAuth()) {
                Response::redirectResponse(
                    route('admin')
                );
            } elseif (isProtectedRoute($matchingRoute) && !isAuth()) {
                Response::redirectResponse(
                    route('root')
                );
            } elseif (!empty($redirect = $matchingRoute->getRedirect())) {
                $response = (new Response())->redirect(
                    route($redirect)
                );
            } else {
                if (!empty($callback = $matchingRoute->getCallback())) {
                    if (is_callable($callback)) {
                        $response = $callback($request);
                    } else {
                        $callback = explode('@', $callback);

                        if (
                            count($callback) === 2
                            && class_exists($class = ('app\Controllers\\' . $callback[0]))
                            && method_exists($controller = new $class(), $callback[1])
                        ) {
                            $response = $controller->{$callback[1]}($request);
                        } else {
                            $response = (new Response())->abort(Response::ROUTING_ERROR_NOT_FOUND);
                        }
                    }
                } else {
                    $response = (new Response())->abort(Response::ROUTING_ERROR_NOT_FOUND);
                }
            }
        } else {
            $response = (new Response())->abort(
                $route_match_found
                    ? Response::ROUTING_ERROR_METHOD_NOT_ALLOWED
                    : Response::ROUTING_ERROR_NOT_FOUND
            );
        }

        if ($response instanceof ExecutableResponseInterface) {
            $response->execute();
        }
    }
}