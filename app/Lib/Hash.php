<?php

namespace app\Lib;

class Hash
{
    /**
     * Compare password with a hash
     *
     * @param string|null $password
     * @param string|null $hash
     * @return bool
     */
    public static function compare(?string $password = null, ?string $hash = null): bool
    {
        if (empty($password) || empty($hash)) {
            return false;
        }

        return password_verify($password, $hash);
    }

    /**
     * Hash password
     *
     * @param string $password
     * @return string
     */
    public static function make(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }
}