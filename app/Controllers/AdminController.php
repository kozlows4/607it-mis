<?php

namespace app\Controllers;

use app\Lib\Request;
use app\Lib\Response;
use app\Lib\Responses\JsonResponse;
use app\Lib\Responses\ViewResponse;
use app\Models\Model;
use app\Models\User;

/**
 * Class AdminController
 *
 * @package app\Controllers
 */
class AdminController
{
    /**
     * Response
     *
     * @var Response $response
     */
    private Response $response;

    /**
     * AdminController constructor.
     */
    public function __construct()
    {
        $this->response = new Response();
    }

    public function admin(): ViewResponse
    {

    }

    public function mis(): ViewResponse
    {

    }

    public function getUsers(Request $request): JsonResponse
    {
        $count = User::getCount(
            $request->post('search')['value']
        );

        return $this->response->json([
            'draw' => $request->post('draw', 0),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,
            'data' => User::get(
                $request->post('search')['value'],
                $request->post('start', Model::OFFSET),
                $request->post('length', Model::LIMIT),
                $request->post('columns')[
                    $request->post('order')[0]['column']
                ]['data'],
                $request->post('order')[0]['dir']
            )
        ]);
    }

    public function addEditUser(Request $request): ViewResponse
    {

    }

    public function createUpdateUser(Request $request): JsonResponse
    {

    }

    public function deleteUser(Request $request): JsonResponse
    {

    }

    public function editProfile(Request $request): ViewResponse
    {

    }

    public function updateProfile(Request $request): JsonResponse
    {

    }
}