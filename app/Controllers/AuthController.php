<?php

namespace app\Controllers;

use app\Lib\Request;
use app\Lib\Response;
use app\Models\User;

/**
 * Class AuthController
 * @package app\Controllers
 */
class AuthController
{
    /**
     * Response
     *
     * @var Response $response
     */
    private Response $response;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * @param Request $request
     */
    public function login(Request $request): void
    {
        $result = User::login($request);

        if ($result) {
            session('user', json_encode($result));

            Response::redirectResponse(route('admin'));
        } else {
            Response::redirectResponse();
        }
    }

    /**
     * Logout user
     */
    public function logout(): void
    {
        User::logout();
    }

    /**
     * @param Request $request
     */
    public function viewAdmin(Request $request): void
    {
        $user = json_decode(session('user'), true);
        $clients = Client::getClients($request);

        include 'resources/views/admin/clients.php';
    }

    /**
     * @param Request $request
     */
    public function deleteClient(Request $request): void
    {
        Client::delete($request->get('id') ?? 0);

        Response::redirectResponse(
            route(
                'admin',
                empty($search = $request->get('search')) ? [] : ['search' => $search]
            )
        );
    }

    /**
     * @param Request $request
     */
    public function editClient(Request $request): void
    {
        $client = Client::get($request->get('id') ?? 0);
        $action = isset($client) ? 'Edit' : 'Add';

        include 'resources/views/admin/client.php';
    }

    /**
     * @param Request $request
     */
    public function updateClient(Request $request): void
    {
        Client::insertUpdate([
            'id' => $request->post('hiddenId'),
            'first_name' => $request->post('firstName'),
            'last_name' => $request->post('lastName'),
            'email' => $request->post('email')
        ]);

        Response::redirectResponse(
            route('admin')
        );
    }
}