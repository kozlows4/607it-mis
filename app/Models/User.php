<?php


namespace app\Models;

use app\Lib\DB;
use app\Lib\Hash;
use app\Lib\Request;
use app\Lib\Response;

/**
 * Class User
 * @package app\Models
 */
class User extends Model
{
    /**
     * @var string $table
     * @see Model::$table
     */
    protected static string $table = 'users';

    /**
     * @param Request $request
     * @return bool|array
     */
    public static function login(Request $request)
    {
        $user = DB::query(
            self::getLoginQuery(),
            [
                'email' => $request->post('email'),
            ]
        )->fetch();

        if ($user && Hash::compare($request->post('password'), $user['password'])) {
            return $user;
        } else {
            return false;
        }
    }

    /**
     * Log out
     */
    public static function logout(): void
    {
        session_unset();
        session_destroy();

        Response::redirectResponse();
    }

    /**
     * Get login query
     *
     * @return string
     */
    private static function getLoginQuery(): string
    {
        return 'SELECT id, first_name, last_name, email, password FROM users WHERE email = :email LIMIT 1';
    }
}