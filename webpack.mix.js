const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public');

if (mix.inProduction()) {
    mix.options({
        postCss: [
            require('autoprefixer')({
                overrideBrowserslist: [
                    'last 100 versions'
                ],
                cascade: false,
                flexbox: "no-2009",
                grid: true
            }),
        ],
        terser: {
            extractComments: false
        }
    });

    mix.version();
}

mix.js('resources/assets/js/app.js', 'public/js/app.js')
    .sass('resources/assets/sass/app.scss', 'public/css/app.css');