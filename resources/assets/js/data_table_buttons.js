/**
 * DataTable buttons helper
 * @constructor
 */
window.DataTableButtons = new (function () {
    /**
     * Render link button
     * @param {string} route
     * @param {string} text
     * @param {string} style
     * @returns {string}
     */
    this.renderLinkButton = (route, text, style) => {
        let button = document.createElement('a');
        button.href = route;
        button.className = 'btn btn-' + style;
        button.textContent = text;

        return button.outerHTML;
    };

    /**
     * Render general-purpose button with ID value
     * @param {int} id
     * @param {string} text
     * @param {string} style
     * @param {string} customClass
     */
    this.renderIdButton = (id, text, style, customClass = '') => {
        let button = document.createElement('button');
        button.className = 'btn btn-' + style + ' ' + customClass;
        button.textContent = text;
        button.setAttribute('data-id', id.toString());

        return button.outerHTML;
    };
});