try {
    //Popper and jQuery
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    //Bootstrap and DataTable
    require('bootstrap');
    require('datatables.net');
    require('datatables.net-bs4');
    require('datatables.net-scroller');
    require('datatables.net-fixedcolumns');
    require('datatables.net-fixedcolumns-bs4');

    //DataTable buttons helper
    require('./data_table_buttons');
} catch (e) {}