<!--
    Used packages:
    Bootstrap 4
    jQuery
    DataTable
-->
<html lang="en">
<head>
    <title>
        MIS
    </title>

    <meta charset="UTF-8">
    <link href="<?php __(mix('css/app.css'))?>" rel="stylesheet" type="text/css">
    <script src="<?php __(mix('js/app.js'))?>"></script>
</head>
<body>
    <?php include $file ?? ''; ?>
</body>
</html>