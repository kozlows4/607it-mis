<div class="modal" tabindex="-1" role="dialog" id="contactModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Success</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="mb-0">
                    Contact information has been updated. What do you want to do next?
                </p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" href="<?php __($route ?? ''); ?>">
                    Go to contacts list
                </a>

                <?php if ($isEdit ?? false): ?>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                        Stay in the page
                    </button>
                <?php else: ?>
                    <button type="button" class="btn btn-primary" onclick="location.reload();">
                        Add another contact
                    </button>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>